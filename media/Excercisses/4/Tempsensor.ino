#include "DHT.h"                
#define DHTPIN 2          // Pin where the DHT22 is connected
#define DHTTYPE DHT22     // Definition, which DHT sensore is used
       
/********************************( Definition of the objekts )********************************/                          
DHT dht(DHTPIN, DHTTYPE);
int red = 9;
int green = 8;

void setup() {
  Serial.begin(9600);
  Serial.println("DHT22 testprogramm");
  dht.begin();
  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
}
void loop() {
  delay(2000);           // Wait a few seconds between measurements.   
                                    
  float h = dht.readHumidity();    // reading of the Humidity and saving it in variable h
  float t = dht.readTemperature(); // reading of Temperatur in °C and saving it in variable t
  
/*********************( check of data is correct )*********************/ 
  if (isnan(h) || isnan(t)) {       
    Serial.println("reading error!");
    return;
  }

  //print the results into the Serial Monitor
  Serial.print("Humidityt: ");
  Serial.print(h);                  // Output Humidity
  Serial.print("%\t");              // Tab
  Serial.print("Temperature: ");
  Serial.print(t);                  // output Temperatur
  Serial.println("C");


  if(t > 30){                       //if temperature is over 30 degree C ,then the red LED emits else the green emits.
    digitalWrite(red,HIGH);
    digitalWrite(green,LOW);
  }
  else{
    digitalWrite(green,HIGH);
    digitalWrite(red,LOW);
  }
}